<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a target="_blank" href="https://www.facebook.com/a.chobi7">Saiful Islam</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 01
    </div>
</footer>