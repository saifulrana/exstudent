<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistraionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registraions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('blood');
            $table->integer('age');
            $table->integer('mobile');
            $table->integer('email');
            $table->string('village');
            $table->string('location');
            $table->string('job');
            $table->string('passing_year');
            $table->longText('image');
            $table->text('bio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registraions');
    }
}
